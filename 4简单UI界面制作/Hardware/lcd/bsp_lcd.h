#ifndef _BSP_LCD_H
#define _BSP_LCD_H

#include "gd32f4xx.h"
#include "systick.h"


#define LCD_RAM_NUMBER  76800
#define LCD_RAM_BYTE_NUMBER  153600
//extern uint16_t LCD_RAM[LCD_RAM_NUMBER];//显示缓存
extern uint16_t *LCD_SHOW_RAM;
extern uint8_t Lcd_Show_Over;

#define Chip_Selection 1   //设置芯片初始化 0为ILI9341  1为ST7789
#define USE_HORIZONTAL 2  //设置横屏或者竖屏显示 0或1为竖屏 2或3为横屏
#if USE_HORIZONTAL==0||USE_HORIZONTAL==1
#define LCD_W 240
#define LCD_H 320
#else
#define LCD_W 320
#define LCD_H 240
#endif

//画笔颜色
#define WHITE            0xFFFF
#define BLACK            0x0000	  
#define BLUE             0x001F  
#define BRED             0XF81F
#define GRED             0XFFE0
#define GBLUE            0X07FF
#define RED              0xF800
#define MAGENTA          0xF81F
#define GREEN            0x07E0
#define CYAN             0x7FFF
#define YELLOW           0xFFE0
#define BROWN            0XBC40 //棕色
#define BRRED            0XFC07 //棕红色
#define GRAY             0X8430 //灰色
#define DARKBLUE         0X01CF	//深蓝色
#define LIGHTBLUE        0X7D7C	//浅蓝色  
#define GRAYBLUE         0X5458 //灰蓝色
#define LIGHTGREEN       0X841F //浅绿色
#define LGRAY            0XC618 //浅灰色(PANNEL),窗体背景色
#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)


#define LCD_SCLK_Clr() gpio_bit_reset(GPIOC,GPIO_PIN_10)//SCL=SCLK
#define LCD_SCLK_Set() gpio_bit_set(GPIOC,GPIO_PIN_10)
#define LCD_MOSI_Clr() gpio_bit_reset(GPIOC,GPIO_PIN_12)//SDA=MOSI
#define LCD_MOSI_Set() gpio_bit_set(GPIOC,GPIO_PIN_12)


#define LCD_RES_Clr()  gpio_bit_reset(GPIOD,GPIO_PIN_3)//RES
#define LCD_RES_Set()  gpio_bit_set(GPIOD,GPIO_PIN_3)
#define LCD_DC_Clr()   gpio_bit_reset(GPIOD,GPIO_PIN_6)//DC
#define LCD_DC_Set()   gpio_bit_set(GPIOD,GPIO_PIN_6)
#define LCD_CS_Clr()   gpio_bit_reset(GPIOD,GPIO_PIN_2)//CS
#define LCD_CS_Set()   gpio_bit_set(GPIOD,GPIO_PIN_2)
#define LCD_BLK_Clr()  gpio_bit_reset(GPIOA,GPIO_PIN_15)//BLK
#define LCD_BLK_Set()  gpio_bit_set(GPIOA,GPIO_PIN_15)




void LCD_GPIO_Init(void);//初始化GPIO
void LCD_Writ_Bus(uint8_t dat);//软件/硬件SPI发送
void LCD_WR_DATA8(uint8_t dat);//写入一个字节
void LCD_WR_DATA(uint16_t dat);//写入两个字节
void LCD_WR_REG(uint8_t dat);//写入一个指令

void LCD_Init(void);//LCD初始化
void LCD_Fill(uint16_t xsta,uint16_t ysta,uint16_t xend,uint16_t yend,uint16_t color);//清屏








void LCD_Address_Set(uint16_t x1,uint16_t y1,uint16_t x2,uint16_t y2);//设置坐标函数


void LCD_Show_Gram(uint16_t *dat);

#endif
