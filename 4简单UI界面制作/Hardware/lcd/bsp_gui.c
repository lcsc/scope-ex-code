#include "bsp_lcd.h"
#include "bsp_gui.h"
#include "exmc_sdram.h"

#include "bsp_bmp.h"
#include "font.h" 

//__attribute__((at(0XC0000000)));
__align(32) uint16_t Show_GramA[LCD_RAM_NUMBER]  __attribute__((at(SDRAM_DEVICE0_ADDR)));  //双显示Buffr 
__align(32) uint16_t Show_GramB[LCD_RAM_NUMBER]  __attribute__((at(SDRAM_DEVICE0_ADDR+LCD_RAM_BYTE_NUMBER)));
uint8_t Show_AB;    //Buffr判断
uint8_t Show_Star;  //是否更新
//波形
int16_t Show_LinA[320];
//FFT波形
int16_t Show_LinB[320];



//LCD的画笔颜色和背景色	   
uint16_t  POINT_COLOR=GRAM_WHITE;	//画笔颜色
uint16_t  BACK_COLOR=GRAM_BLACK;  //背景色 



//显示设置变量
uint8_t adc_power=1;            //开始暂停
uint8_t wav_trigger=1;          //下降沿上升沿
uint8_t ac_dc=0;                //ACDC
uint8_t gather_rate=2;          //增加减少放大倍数
uint8_t adc_speed=3;            //增加减少采样速度
//基准线 软触发线
int8_t  Level=-10,Trigger=10;   //增加减少中心位置//增加减少触发位置
uint16_t Trigger_number;
uint16_t Trigger_data;
uint8_t wav_number=4;           //增加减少波形种类
uint16_t wav_Fps=10000;         //增加减少波形频率
uint8_t fft_on=1;               //FFT

uint16_t fft_n; //缩放
float fft_fps;

///各种显示字符串的位置  zhi'neng
const char *ui_show[30]=
{
    "AC",
    "DC",
    "   2V",
    "   1V",
    " 1/2V",
    " 1/4V",
    " 1/8V",
    "1/16V",
    "1/32V",
    "1/64V",
    "12.5us",
    "  25us",
    "  50us",
    " 100us",
    " 200us",
    " 400us",
    " 800us",
    " 1.6ms",
    " 3.2ms",
    " 6.4ms",
};


//50ms周期
void Lcd_Show_Time(void)
{
    timer_parameter_struct timer_initpara;
    nvic_irq_enable(TIMER3_IRQn, 0, 1); // 设置中断优先级
    rcu_periph_clock_enable(RCU_TIMER3);
    rcu_timer_clock_prescaler_config(RCU_TIMER_PSC_MUL4);
    
    timer_deinit(TIMER3);  // 定时器复位
    /* TIMER2 configuration */
    timer_initpara.prescaler         = 200 - 1;  // 预分频
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE; // 对齐模式
    timer_initpara.counterdirection  = TIMER_COUNTER_UP; // 计数方向
    timer_initpara.period            = 200000000 / 200 / 20 -1; // 周期
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1; // 时钟分频
    timer_initpara.repetitioncounter = 0; // 重复计数器
    timer_init(TIMER3,&timer_initpara);
    
    /* TIMER2 interrupt enable */
    timer_interrupt_enable(TIMER3,TIMER_INT_UP);  // 中断使能
    timer_enable(TIMER3);
    
}



//颜色清屏函数
void Lcd_Gram_Fill(uint16_t *dat,uint16_t color)
{
    uint32_t i;
    uint16_t *buf=dat;
    
    for(i=0;i<76800;i++) *buf++=color;
}
//画竖线
void Lcd_Show_Lin(uint16_t *dat,uint16_t x,uint8_t pointa,uint8_t pointb,uint16_t color)
{
    uint8_t i;
    
    if(pointa >= LCD_H) pointa=LCD_H-1;
    if(pointb >= LCD_H) pointb=LCD_H-1;
    
    if(pointa<pointb)
    {
        for(i=pointa;i<pointb;i++) *(dat+(239-i)*LCD_W+x) = color;
    }
    else if(pointa>pointb)
    {
        for(i=pointb;i<pointa;i++) *(dat+(239-i)*LCD_W+x) = color;
    }
    else *(dat+(239-pointa)*LCD_W+x) = color;
}


////简单显示
//void Lcd_Show_LinA(uint16_t *dat)
//{
//    uint16_t x,y;
//    uint16_t *buf=dat;
////    int16_t *tmp=lin;
//    
////    //清除全部无线条
////    for(y=0;y<240;y++)
////    {
////        for(x=0;x<320;x++) *buf++ = GRAM_BLACK;
////    }
//    
//    //绘制简单线条
//    for(x=0;x<320;x++) *buf++ = GRAM_BLACK;
//    for(y=1;y<240;y++)
//    {
//        *buf++ = GRAM_BLACK;
//        if(y%20 == 0)
//        {
//            for(x=1;x<320;x++) *buf++ = GRAM_GRAY;
//        }
//        else
//        {
//            for(x=1;x<320;x++)
//            {
//                if(x%25 == 0) *buf++ = GRAM_GRAY;
//                else *buf++ = GRAM_BLACK;
//            }
//        }
//    }
//    
//    
//    //绘制波形线段
//    for(x=0;x<319;x++)
//    {
//        Lcd_Show_Lin(dat,x,Show_LinA[x],Show_LinA[x+1],GRAM_GREEN);
//    }
//    Lcd_Show_Lin(dat,x,Show_LinA[319],Show_LinA[319],GRAM_GREEN);
//    
//    
//    
//    //绘制FFT线段
//    for(x=0;x<319;x++)
//    {
//        Lcd_Show_Lin(dat,x,Show_LinB[x],Show_LinB[x+1],GRAM_YELLOW);
//    }
//    Lcd_Show_Lin(dat,x,Show_LinB[319],Show_LinB[319],GRAM_YELLOW);
//    
//    
//}


////绘制黑白取模图片
void Lcd_Show_Bmp(uint16_t *dat,uint8_t *bmp,uint16_t x,uint16_t y,uint16_t w,uint16_t h)
{
    uint16_t *buf;
    uint16_t a,b;
    uint8_t i,temp;
    
    for(a=y;a<y+h;a++)
    {
        buf = dat + a*320 + x;
        for(b=x;b<x+w;b+=8)
        {
            temp = *bmp++;
            if(b+8 > x+w)
            {
                for(i=0;i<(x+w-b);i++)
                {
                    if(temp & 0x80) *buf++ = POINT_COLOR;
                    else *buf++ = BACK_COLOR;
                    temp = temp<<1;
                }
            }
            else
            {
                for(i=0;i<8;i++)
                {
                    if(temp & 0x80) *buf++ = POINT_COLOR;
                    else *buf++ = BACK_COLOR;
                    temp = temp<<1;
                }
            }
        }
    }
}


/////画点
void LTDC_Draw_Point(uint16_t *dat, uint16_t x, uint16_t y, uint32_t color)
{
  *(dat + (LCD_W * (y) + x)) = color;
}
//在指定位置显示一个字符
void LCD_ShowChar(uint16_t *dat,uint16_t x,uint16_t y,uint8_t num,uint8_t size,uint8_t mode)
{  
    uint8_t temp,t1,t;
    uint16_t y0=y;
    uint8_t csize=(size/8+((size%8)?1:0))*(size/2);		//得到字体一个字符对应点阵集所占的字节数	
    num=num-' ';//得到偏移后的值（ASCII字库是从空格开始取模，所以-' '就是对应字符的字库）
    for(t=0;t<csize;t++)
    {   
        if(size==12)temp=asc2_1206[num][t]; 	 	//调用1206字体
        else if(size==16)temp=asc2_1608[num][t];	//调用1608字体
        else if(size==24)temp=asc2_2412[num][t];	//调用2412字体
        else return;								//没有的字库
        for(t1=0;t1<8;t1++)
        {			    
            if(temp&0x80)LTDC_Draw_Point(dat,x,y,POINT_COLOR);
            else if(mode==0)LTDC_Draw_Point(dat,x,y,BACK_COLOR);
            temp<<=1;
            y++;
            if(y>=LCD_H)return;		//超区域了
            if((y-y0)==size)
            {
                y=y0;
                x++;
                if(x>=LCD_W)return;	//超区域了
                break;
            }
        }  	 
    }  
}   
//显示字符串
void LCD_ShowString(uint16_t *dat,uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t mode,uint8_t *p)
{         
	uint8_t x0=x;
	width+=x;
	height+=y;
    while((*p<='~')&&(*p>=' '))//判断是不是非法字符!
    {       
        if(x>=width){x=x0;y+=size;}
        if(y>=height)break;//退出
        LCD_ShowChar(dat,x,y,*p,size,mode);
        x+=size/2;
        p++;
    }  
}

//m^n函数
//返回值:m^n次方.
uint32_t LCD_Pow(uint8_t m,uint8_t n)
{
	uint32_t result=1;	 
	while(n--)result*=m;    
	return result;
}

//显示数字,高位为0,则不显示
void LCD_ShowNum(uint16_t *dat,uint16_t x, uint16_t y, uint32_t num, uint8_t len, uint8_t size,uint8_t mode)
{
    uint8_t t, temp;
    uint8_t enshow = 0;

    for (t = 0; t < len; t++)
    {
        temp = (num / LCD_Pow(10, len - t - 1)) % 10;
        if (enshow == 0 && t < (len - 1))
        {
            if (temp == 0)
            {
                LCD_ShowChar(dat, x + (size / 2)*t, y, ' ', size, mode);
                continue;
            }
            else enshow = 1;
        }
        LCD_ShowChar(dat, x + (size / 2)*t, y, temp + '0', size, mode);
    }
}




////显示ADC和FFT波形
void Lcd_Show_Wav(uint16_t *dat)
{
    uint16_t x;
    uint16_t *tmp;
    

    //位置框
    POINT_COLOR=GRAM_WHITE;	//画笔颜色
    Lcd_Show_Bmp(dat,(uint8_t *)gImage_1,30,2,88,16); //缓冲区
    //位置浮标
    if(adc_power) POINT_COLOR=GRAM_WHITE;	//画笔颜色
    else POINT_COLOR=GRAM_LIGHTBLUE;	//画笔颜色    
    Lcd_Show_Bmp(dat,(uint8_t *)gImage_2,38+(Trigger_number*62/1680),7,11,6);//进度条  74-11
    LCD_ShowNum(dat,240,222,Trigger_number,4,16,0);
    //FFT
    POINT_COLOR=GRAM_WHITE;	//画笔颜色
    LCD_ShowNum(dat,140,222,fft_fps,5,16,0); //频率
    LCD_ShowNum(dat,190,222,fft_n,5,16,0); //缩放倍数
    //绘制黑白取模栅格
    POINT_COLOR=GRAM_GRAYA;	//画笔颜色
    Lcd_Show_Bmp(dat,(uint8_t *)gImage_b,10,20,301,201);
    
    //绘制触发导线
    tmp = dat + (120-Trigger)*320 + 10;
    for(x=0;x<101;x++)
    {
        *tmp = GRAM_BLUE;
        tmp += 3;
    }
    //绘制偏移导线
    tmp = dat + (120-Level)*320 + 10;
    for(x=0;x<61;x++)
    {
        *tmp = GRAM_WHITE;
        tmp += 5;
    }

    //绘制FFT波形线段
    for(x=0;x<300;x++) 
    {
        Lcd_Show_Lin(dat,11+x,Show_LinB[x],Show_LinB[x+1],GRAM_YELLOW);
    }
    //绘制ADC波形线段
    for(x=11;x<310;x++) 
    {
        Lcd_Show_Lin(dat,x,Show_LinA[x],Show_LinA[x+1],GRAM_GREEN);
    }
    POINT_COLOR=GRAM_WHITE;	//画笔颜色
    
}


////控制对应统一刷新函数
void Lcd_Show_Data(uint8_t Number)
{
    switch(Number)
    {
        
        case 0:
        {   //开始暂停
            if(!adc_power) POINT_COLOR=GRAM_LIGHTBLUE;	//画笔颜色
            Lcd_Show_Bmp(Show_GramA,(uint8_t *)gImage_4[2+adc_power],10,2,16,16);//开始暂停
            Lcd_Show_Bmp(Show_GramB,(uint8_t *)gImage_4[2+adc_power],10,2,16,16);//开始暂停
            POINT_COLOR=GRAM_WHITE;	//画笔颜色
            break;
        }
        case 1:
        {   //下降沿上升沿
            Lcd_Show_Bmp(Show_GramA,(uint8_t *)gImage_4[0+wav_trigger],125,2,16,16);
            Lcd_Show_Bmp(Show_GramB,(uint8_t *)gImage_4[0+wav_trigger],125,2,16,16);
            break;
        }
        case 2:
        {   //ACDC
            LCD_ShowString(Show_GramA,150,2,320,20,16,0,(uint8_t *)ui_show[ac_dc]); 
            LCD_ShowString(Show_GramB,150,2,320,20,16,0,(uint8_t *)ui_show[ac_dc]); 
            break;
        }
        case 3:
        {   //增加减少放大倍数
            LCD_ShowString(Show_GramA,175,2,320,20,16,0,(uint8_t *)ui_show[2+gather_rate]);
            LCD_ShowString(Show_GramB,175,2,320,20,16,0,(uint8_t *)ui_show[2+gather_rate]);
            break;
        }
        case 4:
        {   //增加减少采样速度
            LCD_ShowString(Show_GramA,240,2,320,20,16,0,(uint8_t *)ui_show[10+adc_speed]);
            LCD_ShowString(Show_GramB,240,2,320,20,16,0,(uint8_t *)ui_show[10+adc_speed]);
            break;
        }
        case 5:
        {   //增加减少触发位置
            //POINT_COLOR=GRAM_BLUE;	//画笔颜色
            Lcd_Show_Bmp(Show_GramA,(uint8_t *)gImage_t[1],311,117-Trigger,8,8);
            Lcd_Show_Bmp(Show_GramB,(uint8_t *)gImage_t[1],311,117-Trigger,8,8);
            break;
        }
        case 6:
        {   //增加减少中心位置
            //POINT_COLOR=GRAM_WHITE;	//画笔颜色
            Lcd_Show_Bmp(Show_GramA,(uint8_t *)gImage_t[0],0,117-Level,8,8);
            Lcd_Show_Bmp(Show_GramB,(uint8_t *)gImage_t[0],0,117-Level,8,8);
            break;
        }
        case 7:
        {   //增加减少波形种类
            Lcd_Show_Bmp(Show_GramA,(uint8_t *)gImage_3[wav_number],10,222,32,16);
            Lcd_Show_Bmp(Show_GramB,(uint8_t *)gImage_3[wav_number],10,222,32,16);
            break;
        }
        case 8:
        {   //增加减少波形频率
            LCD_ShowNum(Show_GramA,50,222,wav_Fps,5,16,0);
            LCD_ShowNum(Show_GramB,50,222,wav_Fps,5,16,0);
            break;
        }
        case 9:
        {   //FFT功能
            Lcd_Show_Bmp(Show_GramA,(uint8_t *)gImage_3[5+fft_on],100,222,32,16);
            Lcd_Show_Bmp(Show_GramB,(uint8_t *)gImage_3[5+fft_on],100,222,32,16);
            break;
        }
        default:
            break;
    }
}

