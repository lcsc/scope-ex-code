#ifndef _BSP_ADC_H
#define _BSP_ADC_H


#include "gd32f4xx.h"
#include "systick.h"


//交流直流设置
#define alternating         0
#define direct              1
//采样倍率设置   40点/
#define gather_rate_1_2      0
#define gather_rate_1      1
#define gather_rate_2        2
#define gather_rate_4        3
#define gather_rate_8        4
#define gather_rate_16        5
#define gather_rate_32       6
#define gather_rate_64       7
//采样速度设置 
#define adc_speed_2M        0
#define adc_speed_1M        1
#define adc_speed_500K      2
#define adc_speed_250K      3
#define adc_speed_125K      4
#define adc_speed_min      9



#define adc_buff_max    4000
#define adc_buff_2x     2000
#define adc_on  1
#define adc_off 0



extern uint16_t adc_value[adc_buff_max];
extern uint8_t adc_dma_AB;
extern uint8_t adc_dma_ok;
extern uint16_t adc_buff[adc_buff_2x];


void adc_config(void);
void adc_setio_init(void);
void alternating_direct_set(unsigned char gather);
void gather_rate_set(unsigned char rate);
void adc_speed_set(unsigned char speed);
void adc_power_set(unsigned char onoff);//采样开始停止设置


#endif
