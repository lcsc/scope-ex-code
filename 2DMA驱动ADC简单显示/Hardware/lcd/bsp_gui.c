#include "bsp_lcd.h"
#include "bsp_gui.h"
#include "exmc_sdram.h"


//__attribute__((at(0XC0000000)));
__align(32) uint16_t Show_GramA[LCD_RAM_NUMBER]  __attribute__((at(SDRAM_DEVICE0_ADDR)));  //双显示Buffr 
__align(32) uint16_t Show_GramB[LCD_RAM_NUMBER]  __attribute__((at(SDRAM_DEVICE0_ADDR+LCD_RAM_BYTE_NUMBER)));
uint8_t Show_AB;    //Buffr判断
uint8_t Show_Star;  //是否更新
//波形
uint16_t Show_Lin[320];

//50ms周期
void Lcd_Show_Time(void)
{
    timer_parameter_struct timer_initpara;
    nvic_irq_enable(TIMER3_IRQn, 0, 1); // 设置中断优先级
    rcu_periph_clock_enable(RCU_TIMER3);
    rcu_timer_clock_prescaler_config(RCU_TIMER_PSC_MUL4);
    
    timer_deinit(TIMER3);  // 定时器复位
    /* TIMER2 configuration */
    timer_initpara.prescaler         = 200 - 1;  // 预分频
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE; // 对齐模式
    timer_initpara.counterdirection  = TIMER_COUNTER_UP; // 计数方向
    timer_initpara.period            = 200000000 / 200 / 20 -1; // 周期
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1; // 时钟分频
    timer_initpara.repetitioncounter = 0; // 重复计数器
    timer_init(TIMER3,&timer_initpara);
    
    /* TIMER2 interrupt enable */
    timer_interrupt_enable(TIMER3,TIMER_INT_UP);  // 中断使能
    timer_enable(TIMER3);
    
}



//颜色清屏函数
void Lcd_Gram_Fill(uint16_t *dat,uint16_t color)
{
    uint32_t i;
    uint16_t *buf=dat;
    
    for(i=0;i<76800;i++) *buf++=color;
}
//画点
void Lcd_Show_Point(uint16_t x,uint16_t y,uint16_t *dat,uint16_t color)
{
    *(dat+y*LCD_W+x) = color;
}
//画竖线
void Lcd_Show_Lin(uint16_t *dat,uint16_t x,uint8_t pointa,uint8_t pointb,uint16_t color)
{
    uint8_t i;
    
    if(pointa >= LCD_H) pointa=LCD_H-1;
    if(pointb >= LCD_H) pointb=LCD_H-1;
    
    if(pointa<pointb)
    {
        for(i=pointa;i<pointb;i++) *(dat+(239-i)*LCD_W+x) = color;
    }
    else if(pointa>pointb)
    {
        for(i=pointb;i<pointa;i++) *(dat+(239-i)*LCD_W+x) = color;
    }
    else *(dat+(239-pointa)*LCD_W+x) = color;
}


//简单显示
void Lcd_Show_LinA(uint16_t *dat)
{
    uint16_t x,y;
    uint16_t *buf=dat;
//    int16_t *tmp=lin;
    
//    //清除全部无线条
//    for(y=0;y<240;y++)
//    {
//        for(x=0;x<320;x++) *buf++ = GRAM_BLACK;
//    }
    
    //绘制简单线条
    for(x=0;x<320;x++) *buf++ = GRAM_BLACK;
    for(y=1;y<240;y++)
    {
        *buf++ = GRAM_BLACK;
        if(y%20 == 0)
        {
            for(x=1;x<320;x++) *buf++ = GRAM_GRAY;
        }
        else
        {
            for(x=1;x<320;x++)
            {
                if(x%25 == 0) *buf++ = GRAM_GRAY;
                else *buf++ = GRAM_BLACK;
            }
        }
    }
    
    
    //绘制线段
    for(x=0;x<319;x++)
    {
        Lcd_Show_Lin(dat,x,Show_Lin[x],Show_Lin[x+1],GRAM_GREEN);
    }
    Lcd_Show_Lin(dat,x,Show_Lin[319],Show_Lin[319],GRAM_GREEN);
    
    
}






