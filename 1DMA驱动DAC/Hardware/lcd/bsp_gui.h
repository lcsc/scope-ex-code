#ifndef _BSP_GUI_H
#define _BSP_GUI_H

#include "gd32f4xx.h"
#include "systick.h"

extern uint16_t Show_GramA[LCD_RAM_NUMBER];
extern uint16_t Show_GramB[LCD_RAM_NUMBER];
extern uint8_t Show_AB;    //Buffr判断
extern uint8_t Show_Star;  //是否更新


//画笔颜色
#define GRAM_WHITE            0xFFFF
#define GRAM_BLACK            0x0000  
#define GRAM_BLUE             0x1F00  
#define GRAM_BRED             0X1FF8
#define GRAM_GRED             0XE0FF
#define GRAM_GBLUE            0XFF07
#define GRAM_RED              0x00F8
#define GRAM_MAGENTA          0x1FF8
#define GRAM_GREEN            0xE007
#define GRAM_CYAN             0xFF7F
#define GRAM_YELLOW           0xE0FF
#define GRAM_BROWN            0X40BC //棕色
#define GRAM_BRRED            0X07FC //棕红色
#define GRAM_GRAY             0X3084 //灰色
#define GRAM_DARKBLUE         0XCF01 //深蓝色
#define GRAM_LIGHTBLUE        0X7C7D //浅蓝色  
#define GRAM_GRAYBLUE         0X5854 //灰蓝色
#define GRAM_LIGHTGREEN       0X1F84 //浅绿色
#define GRAM_LGRAY            0X18C6 //浅灰色(PANNEL),窗体背景色
#define GRAM_LGRAYBLUE        0X51A6 //浅灰蓝色(中间层颜色)
#define GRAM_LBBLUE           0X122B //浅棕蓝色(选择条目的反色)


void Lcd_Show_Time(void);

void Lcd_Gram_Fill(uint16_t *dat,uint16_t color);





#endif
