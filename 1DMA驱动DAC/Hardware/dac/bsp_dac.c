#include "bsp_dac.h"
#include "bsp_wav.h"

uint8_t convertarr[100]; //__attribute__((at(0X20020000)));  //内部地址高扇区RAM

void dac_config(void)
{
    dma_single_data_parameter_struct dma_struct;
    
    //GPIO使能
    rcu_periph_clock_enable(RCU_GPIOA);
    gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_5);
    
    //DAC使能
    rcu_periph_clock_enable(RCU_DAC);
    dac_deinit();
    dac_trigger_source_config(DAC1, DAC_TRIGGER_T6_TRGO);
    dac_trigger_enable(DAC1);
    dac_wave_mode_config(DAC1, DAC_WAVE_DISABLE);
    dac_output_buffer_enable(DAC1);
    /* enable DAC1 and DMA for DAC1 */
    dac_enable(DAC1);
    dac_dma_enable(DAC1);
    
    //DMA使能
    rcu_periph_clock_enable(RCU_DMA0);
    dma_channel_subperipheral_select(DMA0, DMA_CH6, DMA_SUBPERI7);
    dma_struct.periph_addr         = (uint32_t)&DAC1_R8DH;
    dma_struct.memory0_addr        = (uint32_t)convertarr;
    dma_struct.direction           = DMA_MEMORY_TO_PERIPH;
    dma_struct.number              = 100;
    dma_struct.periph_inc          = DMA_PERIPH_INCREASE_DISABLE;
    dma_struct.memory_inc          = DMA_MEMORY_INCREASE_ENABLE;
    dma_struct.periph_memory_width = DMA_PERIPH_WIDTH_8BIT;
    dma_struct.priority            = DMA_PRIORITY_ULTRA_HIGH;
    dma_struct.circular_mode       = DMA_CIRCULAR_MODE_ENABLE;
    dma_single_data_mode_init(DMA0, DMA_CH6, &dma_struct);
    dma_channel_enable(DMA0, DMA_CH6);
    
    //定时器使能
    rcu_periph_clock_enable(RCU_TIMER6);
    timer_prescaler_config(TIMER6, 9, TIMER_PSC_RELOAD_UPDATE);
    timer_autoreload_value_config(TIMER6, 0xff);
    timer_master_output_trigger_source_select(TIMER6, TIMER_TRI_OUT_SRC_UPDATE);
    timer_enable(TIMER6);
    
    
    
    
//    
//    /////DAC0
//    gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_4);
    
//    rcu_periph_clock_enable(RCU_DAC);
//    dac_deinit();
//    dac_trigger_source_config(DAC0, DAC_TRIGGER_T5_TRGO);
//    dac_trigger_enable(DAC0);
//    dac_wave_mode_config(DAC0, DAC_WAVE_DISABLE);
//    dac_output_buffer_enable(DAC0);
//    /* enable DAC1 and DMA for DAC1 */
//    dac_enable(DAC0);
//    dac_dma_enable(DAC0);
//    
//    rcu_periph_clock_enable(RCU_DMA0);
//    /* configure the DMA0 channel 6 */
//    dma_channel_subperipheral_select(DMA0, DMA_CH5, DMA_SUBPERI7);
//    dma_struct.periph_addr         = (uint32_t)&DAC0_R8DH;
//    dma_struct.memory0_addr        = (uint32_t)convertarr;
//    dma_struct.direction           = DMA_MEMORY_TO_PERIPH;
//    dma_struct.number              = 10;
//    dma_struct.periph_inc          = DMA_PERIPH_INCREASE_DISABLE;
//    dma_struct.memory_inc          = DMA_MEMORY_INCREASE_ENABLE;
//    dma_struct.periph_memory_width = DMA_PERIPH_WIDTH_8BIT;
//    dma_struct.priority            = DMA_PRIORITY_ULTRA_HIGH;
//    dma_struct.circular_mode       = DMA_CIRCULAR_MODE_ENABLE;
//    dma_single_data_mode_init(DMA0, DMA_CH5, &dma_struct);
//    dma_channel_enable(DMA0, DMA_CH5);
//    
//    /* configure the TIMER5 */
//    rcu_periph_clock_enable(RCU_TIMER5);
//    timer_prescaler_config(TIMER5, 0xf, TIMER_PSC_RELOAD_UPDATE);
//    timer_autoreload_value_config(TIMER5, 0xffFF);
//    timer_master_output_trigger_source_select(TIMER5, TIMER_TRI_OUT_SRC_UPDATE);
//    timer_enable(TIMER5);
    
}


//1-50K
void Dac_Time_Hz(uint16_t hz)
{
    if(hz>max_wav_fps) hz=max_wav_fps;
    
    if(hz>1000) 
    {
        TIMER_PSC(TIMER6) = 0;
        TIMER_CAR(TIMER6) = 200000000 / 100 / hz -1;
    }
    else if(hz>10) 
    {
        TIMER_PSC(TIMER6) = 10-1;
        TIMER_CAR(TIMER6) = 200000000 / 1000 / hz -1;
    }
    else
    {
        TIMER_PSC(TIMER6) = 50-1;
        TIMER_CAR(TIMER6) = 200000000 / 5000 / hz -1;
    }
}

//设置波形
void Dac_Show_Wav(uint8_t number)
{
    uint8_t i;
    if(number>max_wav_number-1) number=max_wav_number-1;
    for(i=0;i<100;i++) convertarr[i]=wav_data[number][i];
}


