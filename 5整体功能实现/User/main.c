
#include "gd32f4xx.h"
#include "systick.h"
#include <stdio.h>
#include "main.h"
#include "bsp_led.h"
#include "sys.h"
#include "bsp_usart.h"
#include "bsp_key.h"
#include "bsp_basic_timer.h"

#include "bsp_lcd.h"
#include "bsp_spi.h"
#include "bsp_gui.h"
#include "exmc_sdram.h"

#include "bsp_dac.h"
#include "bsp_adc.h"

#include "arm_math.h"  


//ADC采集数据指针
uint16_t *adc_tmp;

//FFT变量
#define FFT_LENGTH		1024 		//FFT长度，默认是1024点FFT
float fft_inputbuf[FFT_LENGTH*2];	//FFT输入数组
float fft_outputbuf[FFT_LENGTH];	//FFT输出数组
arm_cfft_radix4_instance_f32 scfft;

//FFT显示处理
float max_fft;
uint16_t fft_number;
//uint16_t fft_n; //缩放比例

uint16_t Lase_Trigger_number;
uint8_t show_updata;
uint16_t data_tmp;

//波轮按键对应设置
uint8_t keya_set=0;
uint8_t keya_number=3;
uint8_t keyb_set=0;
uint8_t keyb_number=4;

/*!
    \brief    main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    uint32_t i;
    
    nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);  // 优先级分组
    systick_config();
    led_gpio_config();  // led初始化
    key_gpio_config(); // key初始化
    usart_gpio_config(9600U);
    
    //sram初始化
    exmc_synchronous_dynamic_ram_init(EXMC_SDRAM_DEVICE0);
    
    //LCD初始化
    LCD_Init();
    //LCD_Fill(0,0,LCD_W,LCD_H,WHITE);//50ms
    Spi2_Dma_Init();
    Lcd_Gram_Bmp((uint8_t *)Show_GramA);
    LCD_Show_Gram(Show_GramA);
    while(Lcd_Show_Over);
    LCD_BLK_Set();//打开背光
    delay_1ms(800);
    Lcd_Gram_Fill(Show_GramA,GRAM_BLACK);
    Lcd_Gram_Fill(Show_GramB,GRAM_BLACK);
    
    
    //开启定时器固定刷屏
    Lcd_Show_Time();
    
    //波形输出
    dac_config();
    Dac_Show_Wav(wav_number);
    Dac_Time_Hz(wav_Fps);
    
    //ADC采集
    adc_config();
    adc_setio_init();
    alternating_direct_set(ac_dc);
    gather_rate_set(gather_rate);
    adc_speed_set(adc_speed);
    
    
    ////FFT初始化 scfft结构体
    arm_cfft_radix4_init_f32(&scfft,FFT_LENGTH,0,1);
    
    //绘制功能图标
    POINT_COLOR=GRAM_WHITE;	//画笔颜色
    for(i=0;i<10;i++) Lcd_Show_Data(i);
    POINT_COLOR=GRAM_YELLOW;	//画笔颜色
    Lcd_Show_Data(keya_number);
    POINT_COLOR=GRAM_WHITE;	//画笔颜色
    POINT_COLOR=GRAM_GREEN;	//画笔颜色
    Lcd_Show_Data(keyb_number);
    POINT_COLOR=GRAM_WHITE;	//画笔颜色
    
    while(1) 
    {
        //定时器循环固定数据刷屏
        while(Lcd_Show_Over);
        if(show_updata)
        {
            show_updata=0;
            Show_Star=1;            //开始这个一帧标志
            while(Show_Star);       //等待帧处理
        }
        
        
        if(adc_power)
        {
            //等待采集完成
            while(!adc_dma_ok);
            adc_dma_ok=0;
            
            //配置指针 添加计算缓冲  DMA采样速度过快
            if(adc_dma_AB) adc_tmp = adc_value + adc_buff_2x;
            else adc_tmp = adc_value ;
            for(i=0;i<adc_buff_2x;i++) adc_buff[i] = adc_tmp[i];
            
            //FFT运算 <1ms
            for(i=0;i<FFT_LENGTH;i++)//生成信号序列
            {
                fft_inputbuf[2*i]=adc_buff[i];//生成输入信号实部
                 fft_inputbuf[2*i+1]=0;//虚部全部为0
            }
            arm_cfft_radix4_f32(&scfft,fft_inputbuf);	//FFT计算（基4）
            arm_cmplx_mag_f32(fft_inputbuf,fft_outputbuf,FFT_LENGTH);	//把运算结果复数求模得幅值 
            fft_outputbuf[0] = 0;//第一个值非常大，这里舍弃
            //测算最大FFT点
            max_fft = 0;
            fft_number=0;
            for(i=0;i<320;i++)
            {
                if(fft_outputbuf[i] > max_fft)
                {
                    max_fft = fft_outputbuf[i];
                    fft_number = i;
                }
            }
            //自动评估FFT缩放等级
            fft_n = max_fft/150 + 1; //只显示150个刻度
            for(i=0;i<320;i++)
            {
                Show_LinB[i]=(fft_outputbuf[i]/fft_n +27);
                if(Show_LinB[i] > 227) Show_LinB[i]=227-8;
                else if(Show_LinB[i] < 27) Show_LinB[i]=27-8;
                else Show_LinB[i]=Show_LinB[i]-8;
            }
            //FFT频率估测
            fft_fps = (2000000 / (0x01<<adc_speed)) * fft_number / 1024;
            
            
            //////设定值判定  Trigger  上升下降沿
            Trigger_number=0;
            max_data = 2048+(Trigger-Level)*16;
            if(wav_trigger)
            {
                for(i=lin_stat_set;i<lin_over_set;i++) 
                {
                    if(adc_tmp[i] <  max_data-25) 
                    {
                        for(;i<lin_over_set;i++) 
                        {
                            if(adc_tmp[i] > max_data)
                            {
                                Trigger_number=i-lin_stat_set;
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            else
            {
                for(i=lin_stat_set;i<lin_over_set;i++) 
                {
                    if(adc_tmp[i] >  max_data+25) 
                    {
                        for(;i<lin_over_set;i++) 
                        {
                            if(adc_tmp[i] < max_data)
                            {
                                Trigger_number=i-lin_stat_set;
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            //没有触发的情况
            if(!Trigger_number)
            {
                //确定最大/小值
                min_data=0xffff;
                max_data=0;
                for(i=lin_stat_set;i<lin_over_set;i++) 
                {
                    if(adc_tmp[i]< min_data) min_data=adc_tmp[i];
                    if(adc_tmp[i]> max_data) max_data=adc_tmp[i];
                }
                //最低确定位置 
                data_tmp = (max_data + min_data)/2;
                for(i=lin_stat_set;i<lin_over_set;i++) 
                {
                    if(adc_tmp[i] >  data_tmp) 
                    {
                        for(;i<lin_over_set;i++) 
                        {
                            if(adc_tmp[i] < min_data+25)
                            {
                                Trigger_number=i-lin_stat_set;
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            
            
            
            
            
            //缓存缩放裁剪导入
            for(i=0;i<320;i++)
            {
                Show_LinA[i]=((adc_buff[i+Trigger_number] + 0x08)>>4) + Level;//
                if(Show_LinA[i] > 227) Show_LinA[i]=227-8;
                else if(Show_LinA[i] < 27) Show_LinA[i]=27-8;
                else Show_LinA[i]=Show_LinA[i]-8;
            }
            
            //显示数据
            if(Show_AB)  Lcd_Show_Wav(Show_GramA);
            else Lcd_Show_Wav(Show_GramB);
            
            //按键配置
            if(key[0] == Key_Time)
            {
                key[0] = Key_No; //需要再次释放
                Key_Make_Set(6); //ACDC
            }
            if(key[1] == Key_Time)
            {
                key[1] = Key_No; //需要再次释放
                Lase_Trigger_number = Trigger_number+1;
                Key_Make_Set(0); //开始暂停
            }
            if(key[2] == Key_Time)
            {
                key[2] = Key_No; //需要再次释放
                Key_Make_Set(27); //FFT
            }
            //拨轮A
            if(key[3] == Key_Time)
            {
                if(keya_set)
                {
                    key[3] = Key_No; //需要再次释放
                    Lcd_Show_Data(keya_number);
                    if(keya_number < 9) keya_number++;
                    POINT_COLOR=GRAM_YELLOW;	//画笔颜色
                    Lcd_Show_Data(keya_number);
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
                else
                {
                    if(keya_number==5 || keya_number==6 || keya_number==8) key[3]--; //不需要释放
                    else key[3] = Key_No; //需要再次释放
                    POINT_COLOR=GRAM_YELLOW;	//画笔颜色
                    Key_Make_Set(keya_number*3 + 2); //减1
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
            }
            if(key[4] == Key_Time)
            {
                key[4] = Key_No; //需要再次释放
                keya_set = !keya_set;
            }
            if(key[5] == Key_Time)
            {
                if(keya_set)
                {
                    key[5] = Key_No; //需要再次释放
                    Lcd_Show_Data(keya_number);
                    if(keya_number) keya_number--;
                    POINT_COLOR=GRAM_YELLOW;	//画笔颜色
                    Lcd_Show_Data(keya_number);
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
                else
                {
                    if(keya_number==5 || keya_number==6 || keya_number==8) key[5]--; //不需要释放
                    else key[5] = Key_No; //需要再次释放
                    POINT_COLOR=GRAM_YELLOW;	//画笔颜色
                    Key_Make_Set(keya_number*3 + 1); //减1
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
            }
            //拨轮B
            if(key[6] == Key_Time)
            {
                if(keyb_set)
                {
                    key[6] = Key_No; //需要再次释放
                    Lcd_Show_Data(keyb_number);
                    if(keyb_number < 9) keyb_number++;
                    POINT_COLOR=GRAM_GREEN;	//画笔颜色
                    Lcd_Show_Data(keyb_number);
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
                else
                {
                    if(keyb_number==5 || keyb_number==6 || keyb_number==8) key[6]--; //不需要释放
                    else key[6] = Key_No; //需要再次释放
                    POINT_COLOR=GRAM_GREEN;	//画笔颜色
                    Key_Make_Set(keyb_number*3 + 2); //减1
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
            }
            if(key[7] == Key_Time)
            {
                key[7] = Key_No; //需要再次释放
                keyb_set = !keyb_set;
            }
            if(key[8] == Key_Time)
            {
                if(keyb_set)
                {
                    key[8] = Key_No; //需要再次释放
                    Lcd_Show_Data(keyb_number);
                    if(keyb_number) keyb_number--;
                    POINT_COLOR=GRAM_GREEN;	//画笔颜色
                    Lcd_Show_Data(keyb_number);
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
                else
                {
                    if(keyb_number==5 || keyb_number==6 || keyb_number==8) key[8]--; //不需要释放
                    else key[8] = Key_No; //需要再次释放
                    POINT_COLOR=GRAM_GREEN;	//画笔颜色
                    Key_Make_Set(keyb_number*3 + 1); //减1
                    POINT_COLOR=GRAM_WHITE;	//画笔颜色
                }
            }
            
            
            
            //更新显示
            show_updata=1;
        }
        else
        {
            if(Lase_Trigger_number != Trigger_number) 
            {
                Lase_Trigger_number = Trigger_number;
                
                //确定最大/小值
                min_data=0xffff;
                max_data=0;
                for(i=0;i<300;i++) 
                {
                    if(adc_tmp[i+Trigger_number]< min_data) min_data=adc_tmp[i+Trigger_number];
                    if(adc_tmp[i+Trigger_number]> max_data) max_data=adc_tmp[i+Trigger_number];
                }
                //缓存导入
                for(i=0;i<320;i++)
                {
                    Show_LinA[i]=((adc_tmp[i+Trigger_number] + 0x08)>>4) + Level;
                    //Show_LinA[i]=((adc_tmp[i+Trigger_number] )>>4) + Level;
                    if(Show_LinA[i] > 227) Show_LinA[i]=227-8;
                    else if(Show_LinA[i] < 27) Show_LinA[i]=27-8;
                    else Show_LinA[i]=Show_LinA[i]-8;
                }
                //显示数据
                if(Show_AB)
                {
                    //绘制波形
                    Lcd_Show_Wav(Show_GramA);
                }
                else
                {
                    //绘制波形
                    Lcd_Show_Wav(Show_GramB);
                }
                //更新显示
                show_updata=1;
            }
            
            //按键配置
            if(key[0] == Key_Time)
            {
                key[0]--; //不需要再次释放
                if(Trigger_number < adc_buff_2x-320) Trigger_number+=5;
            }
            if(key[1] == Key_Time)
            {
                key[1] = Key_No; //需要再次释放
                //Lase_Trigger_number = Trigger_number+1;
                Key_Make_Set(0); //开始暂停
            }
            if(key[2] == Key_Time)
            {
                key[2]--; //不需要再次释放
                if(Trigger_number > 5) Trigger_number-=5;
                else if(Trigger_number) Trigger_number = 0;
                else;
            }
            //拨轮A
            if(key[3] == Key_Time)
            {
                key[3]--;
                if(Trigger_number < adc_buff_2x-320) Trigger_number++;
            }
            if(key[4] == Key_Time)
            {
                key[4] = Key_No; //需要再次释放
            }
            if(key[5] == Key_Time)
            {
                key[5]--;
                if(Trigger_number) Trigger_number--;
            }
            //拨轮B
            if(key[6] == Key_Time)
            {
                key[6]--;
                if(Trigger_number < adc_buff_2x-320) Trigger_number+=2;
            }
            if(key[7] == Key_Time)
            {
                key[7] = Key_No; //需要再次释放
            }
            if(key[8] == Key_Time)
            {
                key[8]--;
                if(Trigger_number > 2) Trigger_number-=2;
                else if(Trigger_number) Trigger_number = 0;
                else;
            }
            
        }
        
        
        
        
        
        
        
        
        
        

        
        
        //delay_1ms(200);
    }
}


//屏幕刷新 50mS
void TIMER3_IRQHandler(void)
{
    timer_interrupt_flag_clear(TIMER3, TIMER_INT_FLAG_UP);
    if(Show_Star)
    {
        LED1_TOGGLE();
        Show_Star=0;
        if(Show_AB)LCD_Show_Gram(Show_GramA);
        else LCD_Show_Gram(Show_GramB);
        Show_AB =!Show_AB;
    }
    //按键检测
    if(KeyUp) key[0] &= Key_Time;
    else if(key[0] < Key_Time) key[0]++;
    else;
    if(KeyOn) key[1] &= Key_Time;
    else if(key[1] < Key_Time) key[1]++;
    else;
    if(KeyDown) key[2] &= Key_Time;
    else if(key[2] < Key_Time) key[2]++;
    else;
    //拨码A
    if(KeyAUp) key[3] &= Key_Time;
    else if(key[3] < Key_Time) key[3]++;
    else;
    if(KeyAOn) key[4] &= Key_Time;
    else if(key[4] < Key_Time) key[4]++;
    else;
    if(KeyADown) key[5] &= Key_Time;
    else if(key[5] < Key_Time) key[5]++;
    else;
    //拨码B
    if(KeyBUp) key[6] &= Key_Time;
    else if(key[6] < Key_Time) key[6]++;
    else;
    if(KeyBOn) key[7] &= Key_Time;
    else if(key[7] < Key_Time) key[7]++;
    else;
    if(KeyBDown) key[8] &= Key_Time;
    else if(key[8] < Key_Time) key[8]++;
    else;
    
}

//ADC采集
void DMA1_Channel0_IRQHandler(void)
{
    LED3_TOGGLE();
    adc_dma_ok=1;
    if(dma_interrupt_flag_get(DMA1, DMA_CH0,DMA_INT_FLAG_HTF) == SET) adc_dma_AB=0;
    else adc_dma_AB=1;
   dma_interrupt_flag_clear(DMA1, DMA_CH0, DMA_INT_FLAG_FTF|DMA_INT_FLAG_HTF);
}

