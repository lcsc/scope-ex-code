#ifndef _BSP_GUI_H
#define _BSP_GUI_H

#include "gd32f4xx.h"
#include "systick.h"

extern uint16_t Show_GramA[LCD_RAM_NUMBER];
extern uint16_t Show_GramB[LCD_RAM_NUMBER];
extern uint8_t Show_AB;    //Buffr判断
extern uint8_t Show_Star;  //是否更新
extern int16_t Show_LinA[320];
extern int16_t Show_LinB[320];


//LCD的画笔颜色和背景色	   
extern uint16_t  POINT_COLOR;	//画笔颜色
extern uint16_t  BACK_COLOR;  //背景色 
extern int8_t  Level,Trigger;
extern uint16_t Trigger_number;
extern uint16_t Trigger_data;
#define lin_stat_set     160
#define lin_over_set     800


//显示设置变量
extern uint8_t adc_power;   //开始暂停
extern uint8_t wav_trigger; //下降沿上升沿
extern uint8_t ac_dc;       //ACDC
extern uint8_t gather_rate; //增加减少放大倍数
extern uint8_t adc_speed;   //增加减少采样速度
extern uint8_t wav_number;  //增加减少波形种类
extern uint16_t wav_Fps;    //增加减少波形频率
extern uint8_t fft_on;      //FFT

extern uint16_t fft_n; //缩放
extern float fft_fps;

extern uint16_t min_data;
extern uint16_t max_data;

//画笔颜色
#define GRAM_WHITE            0xFFFF
#define GRAM_BLACK            0x0000  
#define GRAM_BLUE             0x1F00  
#define GRAM_BRED             0X1FF8
#define GRAM_GRED             0XE0FF
#define GRAM_GBLUE            0XFF07
#define GRAM_RED              0x00F8
#define GRAM_MAGENTA          0x1FF8
#define GRAM_GREEN            0xE007
#define GRAM_CYAN             0xFF7F
#define GRAM_YELLOW           0xE0FF
#define GRAM_BROWN            0X40BC //棕色
#define GRAM_BRRED            0X07FC //棕红色
#define GRAM_GRAY             0X3084 //灰色
#define GRAM_GRAYA             0X2842 //灰色
#define GRAM_DARKBLUE         0XCF01 //深蓝色
#define GRAM_LIGHTBLUE        0X7C7D //浅蓝色  
#define GRAM_GRAYBLUE         0X5854 //灰蓝色
#define GRAM_LIGHTGREEN       0X1F84 //浅绿色
#define GRAM_LGRAY            0X18C6 //浅灰色(PANNEL),窗体背景色
#define GRAM_LGRAYBLUE        0X51A6 //浅灰蓝色(中间层颜色)
#define GRAM_LBBLUE           0X122B //浅棕蓝色(选择条目的反色)


void Lcd_Show_Time(void);

void Lcd_Gram_Fill(uint16_t *dat,uint16_t color);
void Lcd_Show_Lin(uint16_t *dat,uint16_t x,uint8_t pointa,uint8_t pointb,uint16_t color);
void Lcd_Gram_Bmp(uint8_t *dat);

//void Lcd_Show_LinA(uint16_t *dat);

//字符串
void LTDC_Draw_Point(uint16_t *dat, uint16_t x, uint16_t y, uint32_t color);
void LCD_ShowChar(uint16_t *dat,uint16_t x,uint16_t y,uint8_t num,uint8_t size,uint8_t mode);
void LCD_ShowString(uint16_t *dat,uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t mode,uint8_t *p);
uint32_t LCD_Pow(uint8_t m,uint8_t n);
void LCD_ShowNum(uint16_t *dat,uint16_t x, uint16_t y, uint32_t num, uint8_t len, uint8_t size,uint8_t mode);


//波形栅格 + ADC和FFT波形
void Lcd_Show_Wav(uint16_t *dat);
////控制对应统一刷新函数
void Lcd_Show_Data(uint8_t Number);
////按键对应统一控制函数
void  Key_Make_Set(uint8_t Number);


#endif
