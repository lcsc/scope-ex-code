#ifndef _BSP_KEY_H
#define _BSP_KEY_H

#include "gd32f4xx.h"
#include "systick.h"


//三按键
#define KeyUp       (gpio_input_bit_get(GPIOD,GPIO_PIN_4))
#define KeyOn       (gpio_input_bit_get(GPIOD,GPIO_PIN_5))
#define KeyDown     (gpio_input_bit_get(GPIOC,GPIO_PIN_7))
//拨码A
#define KeyAUp      (gpio_input_bit_get(GPIOC,GPIO_PIN_6))
#define KeyAOn      (gpio_input_bit_get(GPIOG,GPIO_PIN_6))
#define KeyADown    (gpio_input_bit_get(GPIOG,GPIO_PIN_7))
//拨码B
#define KeyBUp      (gpio_input_bit_get(GPIOA,GPIO_PIN_8))
#define KeyBOn      (gpio_input_bit_get(GPIOC,GPIO_PIN_9))
#define KeyBDown    (gpio_input_bit_get(GPIOC,GPIO_PIN_8))

#define Key_Time    2       //判断次数
#define Key_No      0x10    //按键单次触发

extern uint8_t key[9];



void key_gpio_config(void);

#endif



