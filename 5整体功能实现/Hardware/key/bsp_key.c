#include "bsp_key.h"


uint8_t key[9];



void key_gpio_config(void)
{
    rcu_periph_clock_enable(RCU_GPIOD);
    rcu_periph_clock_enable(RCU_GPIOC);
    rcu_periph_clock_enable(RCU_GPIOG);
    rcu_periph_clock_enable(RCU_GPIOA);
    
    gpio_mode_set(GPIOD,GPIO_MODE_INPUT,GPIO_PUPD_PULLUP,GPIO_PIN_4|GPIO_PIN_5);
    gpio_mode_set(GPIOC,GPIO_MODE_INPUT,GPIO_PUPD_PULLUP,GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9);
    gpio_mode_set(GPIOG,GPIO_MODE_INPUT,GPIO_PUPD_PULLUP,GPIO_PIN_6|GPIO_PIN_7);
    gpio_mode_set(GPIOA,GPIO_MODE_INPUT,GPIO_PUPD_PULLUP,GPIO_PIN_8);
}



