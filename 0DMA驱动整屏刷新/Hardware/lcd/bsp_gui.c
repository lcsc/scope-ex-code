#include "bsp_lcd.h"
#include "bsp_gui.h"
#include "exmc_sdram.h"


//__attribute__((at(0XC0000000)));
__align(32) uint16_t Show_GramA[LCD_RAM_NUMBER]  __attribute__((at(SDRAM_DEVICE0_ADDR)));  //双显示Buffr 
__align(32) uint16_t Show_GramB[LCD_RAM_NUMBER]  __attribute__((at(SDRAM_DEVICE0_ADDR+LCD_RAM_BYTE_NUMBER)));
uint8_t Show_AB;    //Buffr判断
uint8_t Show_Star;  //是否更新


//50ms周期
void Lcd_Show_Time(void)
{
    timer_parameter_struct timer_initpara;
    nvic_irq_enable(TIMER3_IRQn, 0, 1); // 设置中断优先级
    rcu_periph_clock_enable(RCU_TIMER3);
    rcu_timer_clock_prescaler_config(RCU_TIMER_PSC_MUL4);
    timer_deinit(TIMER3);  // 定时器复位
    /* TIMER2 configuration */
    timer_initpara.prescaler         = 200 - 1;  // 预分频
    timer_initpara.alignedmode       = TIMER_COUNTER_EDGE; // 对齐模式
    timer_initpara.counterdirection  = TIMER_COUNTER_UP; // 计数方向
    timer_initpara.period            = 200000000 / 200 / 20 -1; // 周期
    timer_initpara.clockdivision     = TIMER_CKDIV_DIV1; // 时钟分频
    timer_initpara.repetitioncounter = 0; // 重复计数器
    timer_init(TIMER3,&timer_initpara);
    
    timer_master_output_trigger_source_select(TIMER3,TIMER_TRI_OUT_SRC_UPDATE);
    /* TIMER2 interrupt enable */
    timer_interrupt_enable(TIMER3,TIMER_INT_UP);  // 中断使能
    timer_enable(TIMER3);
    
}




void Lcd_Gram_Fill(uint16_t *dat,uint16_t color)
{
    uint32_t i;
    uint16_t *buf=dat;
    
    for(i=0;i<76800;i++) *buf++=color;
}




//







