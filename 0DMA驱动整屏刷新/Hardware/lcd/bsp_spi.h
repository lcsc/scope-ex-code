#ifndef _BSP_SPI_H
#define _BSP_SPI_H


#include "gd32f4xx.h"
#include "systick.h"


//////////////////////////APB1 60Mhz
void Spi2_Init(void);
uint8_t Spi2_ReadWriteByte(uint8_t dat);
uint8_t Spi2_Read(void);
void Spi2_Write(uint8_t dat);
void Spi2_Dma_Init(void);




#endif
