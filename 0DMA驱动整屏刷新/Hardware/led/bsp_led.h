#ifndef _BSP_LED_H
#define _BSP_LED_H


#include "gd32f4xx.h"
#include "systick.h"

//#define RCU_LED2        RCU_GPIOD
//#define PORT_LED2       GPIOD
//#define PIN_LED2        GPIO_PIN_7


#define LED1_TOGGLE()     gpio_bit_toggle(GPIOE,GPIO_PIN_3)
#define LED2_TOGGLE()     gpio_bit_toggle(GPIOD,GPIO_PIN_7)
#define LED3_TOGGLE()     gpio_bit_toggle(GPIOG,GPIO_PIN_3)
#define LED4_TOGGLE()     gpio_bit_toggle(GPIOA,GPIO_PIN_5)


void led_gpio_config(void);

#endif
