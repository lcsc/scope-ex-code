
#include "gd32f4xx.h"
#include "systick.h"
#include <stdio.h>
#include "main.h"
#include "bsp_led.h"
#include "sys.h"
#include "bsp_usart.h"
#include "bsp_key.h"
#include "bsp_basic_timer.h"

#include "bsp_lcd.h"
#include "bsp_spi.h"
#include "bsp_gui.h"

#include "exmc_sdram.h"
//#include "malloc.h"




/*!
    \brief    main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
//    uint32_t i;
    
    
    nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);  // 优先级分组
    systick_config();
    led_gpio_config();  // led初始化
    key_gpio_config(); // key初始化
    usart_gpio_config(9600U);
    
    //sram初始化
    exmc_synchronous_dynamic_ram_init(EXMC_SDRAM_DEVICE0);
    
    //LCD初始化
    LCD_Init();
    LCD_Fill(0,0,LCD_W,LCD_H,WHITE);//50ms
    Spi2_Dma_Init();
    Lcd_Gram_Fill(Show_GramA,GRAM_BLUE);
    Lcd_Gram_Fill(Show_GramB,GRAM_RED);
    LCD_Show_Gram(Show_GramA);
    while(Lcd_Show_Over);
    
    //开启定时器固定刷屏
    Lcd_Show_Time();
    
    while(1) 
    {
//        //软件刷屏
//        LCD_Fill(0,0,LCD_W,LCD_H,BLUE);//50ms
//        LCD_Fill(0,0,LCD_W,LCD_H,RED);//50ms
        
//        //循环DMA刷屏
//        Lcd_Gram_Fill(Show_GramA,GRAM_BLUE);
//        LCD_Show_Gram(Show_GramA);
//        while(Lcd_Show_Over);
//        Lcd_Gram_Fill(Show_GramB,GRAM_RED);
//        LCD_Show_Gram(Show_GramB);
//        while(Lcd_Show_Over);
        
        
        //定时器循环固定数据刷屏
        while(Lcd_Show_Over);
        Show_Star=1;
        while(Show_Star);
        
        
        delay_1ms(200);
    }
}



void TIMER3_IRQHandler(void)
{
    LED1_TOGGLE();
    timer_interrupt_flag_clear(TIMER3, TIMER_INT_FLAG_UP);
    if(Show_Star)
    {
        Show_Star=0;
        if(Show_AB)LCD_Show_Gram(Show_GramA);
        else LCD_Show_Gram(Show_GramB);
        Show_AB =!Show_AB;
    }
}

